# Terraform ALB Google Authentication Example

A small example of the Terraform configuration required for setting up Google authentication behind an AWS ALB.

Following the directions in [the AWS docs](https://docs.aws.amazon.com/elasticloadbalancing/latest/application/listener-authenticate-users.html),
on the Google side of things you'll want to create Oauth2 client credentials
for a web application and set the redirect URI appropriately. (For this project,
the redirect URI is https://terraform-alb-auth.drewminnear.com/oauth2/idpresponse).
I grabbed the OIDC endpoints directly from
[Google's OpenID config](https://accounts.google.com/.well-known/openid-configuration).

Once you're setup on the Google side of things, you'll also
need to get an ACM certificate for your loadbalancer and setup
a VPC with at least two public subnets or reuse existing.

Once you use `terraform apply` to spin up the load balancer,
the final step is to make the DNS CNAME record to alias your domain
to the loadbalancer.
