variable "client_id" {
  description = "The client ID from the google credentials page"
}

variable "client_secret" {
  description = "The client secret from the google credentials page"
}

variable "vpc_id" {
  description = "ID of VPC for load balancer"
}

variable "public_subnet_ids" {
  description = "Public subnets in the VPC (must be at least 2) for the load balancer"
  type        = "list"
}

variable "acm_certificate_arn" {
  description = "ARN for an ACM certificate for the domain the ALB is serving"
}

resource "aws_security_group" "lb" {
  name        = "allow_web"
  description = "Allow standard web traffic"
  vpc_id      = "${var.vpc_id}"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_lb" "authenticated" {
  name               = "example-authenticated-lb"
  internal           = false
  load_balancer_type = "application"
  subnets            = "${var.public_subnet_ids}"
  security_groups    = ["${aws_security_group.lb.id}"]
}

resource "aws_lb_listener" "http" {
  load_balancer_arn = "${aws_lb.authenticated.arn}"
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

resource "aws_lb_listener" "https" {
  load_balancer_arn = "${aws_lb.authenticated.arn}"
  port              = "443"
  protocol          = "HTTPS"
  certificate_arn   = "${var.acm_certificate_arn}"

  default_action {
    type = "authenticate-oidc"

    authenticate_oidc {
      authorization_endpoint = "https://accounts.google.com/o/oauth2/v2/auth"
      client_id              = "${var.client_id}"
      client_secret          = "${var.client_secret}"
      issuer                 = "https://accounts.google.com"
      token_endpoint         = "https://oauth2.googleapis.com/token"
      user_info_endpoint     = "https://openidconnect.googleapis.com/v1/userinfo"
    }
  }

  default_action {
    type = "redirect"

    redirect {
      host        = "petstore.swagger.io"
      status_code = "HTTP_301"
    }
  }
}
